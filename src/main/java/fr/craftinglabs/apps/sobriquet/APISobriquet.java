package fr.craftinglabs.apps.sobriquet;

import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

public class APISobriquet {
    private ConcurrentMap<UUID, String> datas;

    public APISobriquet(ConcurrentMap<UUID, String> datas) {
        this.datas = datas;
    }

    public UUID create(String body) {
        UUID id = UUID.randomUUID();
        datas.put(id, body);

        return id;
    }

    public void update(UUID id, String body) {
        if(datas.containsKey(id)) {
            datas.put(id, body);
        } else {
            throw new ElementIdNotFound();
        }
    }

    public String get(UUID id) {
        if(datas.containsKey(id)) {
            return datas.get(id);
        } else {
            throw new ElementIdNotFound();
        }
    }
}

package fr.craftinglabs.apps.sobriquet;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

import static spark.Spark.*;

public class Main {

    public static void main(String[] args) {
        DB db = DBMaker.fileDB("sobriquet.db").make();
        ConcurrentMap datas = db.hashMap("map").make();
        APISobriquet api = new APISobriquet(datas);

        addShutdownHook(db);

        get("/data/:id", (req, res) -> {
            UUID id = UUID.fromString(req.params("id"));
            return api.get(id);
        });

        put("/data/:id", (request, response) -> {
            api.update(UUID.fromString(request.params("id")), request.body());
            return "OK, stored : " + request.body();
        });

        post("/data/", (request, response) -> {
            UUID id = api.create(request.body());
            response.status(200);
            return id.toString();
        });

        exception(ElementIdNotFound.class, (exception, request, response) -> notFound("<html><body><h1>404: id not found</h1></body></html>"));
    }

    private static void addShutdownHook(DB db) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Exiting.");
            stop();
            db.close();
        }, "shutdownHook"));
    }
}

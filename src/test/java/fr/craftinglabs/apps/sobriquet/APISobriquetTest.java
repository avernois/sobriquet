package fr.craftinglabs.apps.sobriquet;

import org.junit.Test;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static org.junit.Assert.*;

public class APISobriquetTest {

    @Test public void
    should_create_an_element_and_return_its_id() {
        ConcurrentMap<UUID, String> map = new ConcurrentHashMap<>();
        APISobriquet api = new APISobriquet(map);

        UUID id = api.create("A string to store");

        assertEquals("A string to store", map.get(id));
    }

    @Test public void
    should_return_value_if_the_id_exist() {
        ConcurrentMap<UUID, String> map = new ConcurrentHashMap<>();
        UUID id = UUID.fromString("d8a27817-1112-46f6-ac1d-631dd74b3581");
        map.put(id, "A stored string");
        APISobriquet api = new APISobriquet(map);

        String value = api.get(id);

        assertEquals("A stored string", value);
    }

    @Test public void
    should_update_value_if_the_id_exist() {
        ConcurrentMap<UUID, String> map = new ConcurrentHashMap<>();
        UUID id = UUID.fromString("d8a27817-1112-46f6-ac1d-631dd74b3581");
        map.put(id, "A stored string");
        APISobriquet api = new APISobriquet(map);

        api.update(id, "An updated value.");

        assertEquals("An updated value.", map.get(id));
    }

    @Test(expected = ElementIdNotFound.class) public void
    should_throw_an_error_when_updating_a_non_existant_id() {
        ConcurrentMap<UUID, String> map = new ConcurrentHashMap<>();
        UUID id = UUID.fromString("d8a27817-1112-46f6-ac1d-631dd74b3581");
        APISobriquet api = new APISobriquet(map);

        api.update(id, "An updated value.");
    }

    @Test(expected = ElementIdNotFound.class) public void
    should_throw_an_error_when_retrieving_a_non_existant_id() {
        ConcurrentMap<UUID, String> map = new ConcurrentHashMap<>();
        UUID id = UUID.fromString("d8a27817-1112-46f6-ac1d-631dd74b3581");
        APISobriquet api = new APISobriquet(map);

        api.get(id);
    }

}